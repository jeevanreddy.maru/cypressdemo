describe('My TestSuite', () => 
{
    it('Verify the title of the Page', () =>
     {
        cy.visit('https://demo.nopcommerce.com/')
        cy.title().should('eq','nopCommerce demo store')
    })    

  })